#!/bin/bash
#
# Script: virtualbox_vm.sh
# Autor: Jonay Garcia
# Fecha: 08/03/2013
# Funcion: Gestion de maquinas virtuales con virtualbox.
##########################################################

VM=""
ACTION=""
VBOXMANAGE_CMD=/usr/bin/vboxmanage

#####################    FUNCIONES    ####################
usage()
{
	echo -e "                                                                                     "
	echo -e "USO: $0 options                                                                      "
	echo -e "                                                                                     "
	echo -e "Este script gestiona una maquina virtual en VIRTUALBOX.                              "
	echo -e "                                                                                     "
	echo -e "OPCIONES:                                                                            "
	echo -e "\t-h: Mostrar ayuda.                                                                 "
	echo -e "\t-m: Maquina virtual.                                                               "
	echo -e "\t-c: Accion a ejecutar: <start | shutdown | poweroff | list | runninglist| status>. "
	echo -e "                                                                                     "
	echo -e "EJEMPLO:                                                                             "
	echo -e "\t- Listar maquinas virtuales:                                                       "
	echo -e "\t\t- $0 -c list.                                                                    "
	echo -e "                                                                                     "
	echo -e "\t- Listar maquinas virtuales que se estan ejecutando:                               "
	echo -e "\t\t- $0 -c runninglist.                                                             "
	echo -e "                                                                                     "
	echo -e "\t- Arrancar una maquina virtual:                                                    "
	echo -e "\t\t- $0 -m <vm> -c start.                                                           "
	echo "                                                                                        "
}

start_vm()
{
        $VBOXMANAGE_CMD startvm $VM
}

shutdown_vm()
{
        $VBOXMANAGE_CMD controlvm $VM acpipowerbutton
}

poweroff_vm()
{
        $VBOXMANAGE_CMD controlvm $VM poweroff
}

list_all_vms()
{
        $VBOXMANAGE_CMD list vms
}

running_list_vms()
{
        $VBOXMANAGE_CMD list runningvms
}

status_vm()
{
        $VBOXMANAGE_CMD showvminfo $VM | grep State
}


#######################  PARAMETROS  #####################

while getopts "hm:c:" opt; do
	case $opt in
		h)
			usage
			exit 1
      			;;
    		m)
      			#echo "-m was triggered!" >&2
			VM=$OPTARG
      			;;
    		c)
      			#echo "-c was triggered!" >&2
			ACTION=$OPTARG
      			;;
    		*)
      			echo "Invalid option: -$OPTARG" >&2
      			;;
  	esac
done


########################    MAIN    ######################

case $ACTION in
	start)
		if [ ! -z $VM ]; then
        		start_vm
		else
			echo "Opcion -m debe estar definida."
			usage
			exit 1
		fi
		;;
	shutdown)
		if [ ! -z $VM ]; then
	        	shutdown_vm
		else
			echo "Opcion -m debe estar definida."
			usage
			exit 1
		fi
		;;
	poweroff)
		if [ ! -z $VM ]; then
	        	poweroff_vm
		else
			echo "Opcion -m debe estar definida."
			usage
			exit 1
		fi
		;;
	list)
		if [ -z $VM ]; then
        		list_all_vms
		else
			echo "Opcion -m no debe estar definida."
			usage
			exit 1
		fi
		;;
	runninglist)
		if [ -z $VM ]; then
        		running_list_vms
		else
			echo "Opcion -m no debe estar definida."
			usage
			exit 1
		fi
		;;
	status)
		if [ ! -z $VM ]; then
        		status_vm
		else
			echo "Opcion -m debe estar definida."
			usage
			exit 1
		fi
		;;
	*)
      		echo "Opcion '$ACTION' no valida." 
		usage
		exit 1
      		;;
esac
