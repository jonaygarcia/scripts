#!/bin/bash
#
# Script: virtualbox_snapshot.sh
# Autor: Jonay Garcia
# Fecha: 08/03/2013
# Funcion: Gestion de maquinas virtuales con virtualbox.
##########################################################

VM=$1
ACTION=$2
SNAPSHOT_NAME=snap

VBOXMANAGE_CMD=/usr/bin/vboxmanage

#####################    FUNCIONES    ####################
usage()
{
        echo -e "                                                                             "
        echo -e "USO: $0 options                                                              "
        echo -e "                                                                             "
        echo -e "Este script gestiona los snapshot de una maquina virtual en VIRTUALBOX.      "
        echo -e "                                                                             "
        echo -e "OPCIONES:                                                                    "
        echo -e "\t-h: Mostrar ayuda.                                                         "
        echo -e "\t-m: Maquina virtual.                                                       "
        echo -e "\t-c: Accion a ejecutar: <take | delete | restore | list>.                   "
        echo -e "                                                                             "
        echo -e "EJEMPLO:                                                                     "
        echo -e "\t- Listar los snapshot de una maquina virtual:                              "
        echo -e "\t\t- $0 -c list -m <vm>.                                                    "
        echo -e "                                                                             "
        echo -e "\t- Tomar un snapshot de una maquina virtual:                                "
        echo -e "\t\t- $0 -m <vm> -c take.                                                    "
        echo -e "                                                                             "
        echo -e "\t- Restaurar al ultimo snapshot:                                            "
        echo -e "\t\t- $0 -m <vm> -c restore.                                                   "
        echo "                                                                                "
}

take_snapshot()
{
	#COUNT_SNAPSHOT=`/usr/bin/vboxmanage snapshot $VM list | grep UUID | wc -l`
	SNAPSHOT_COUNT=`$VBOXMANAGE_CMD snapshot $VM list | grep UUID | wc -l`
	SNAPSHOT_COUNT=$(($SNAPSHOT_COUNT+1))
	$VBOXMANAGE_CMD snapshot $VM take ${SNAPSHOT_NAME}${SNAPSHOT_COUNT} --pause
}

restore_snapshot()
{
	$VBOXMANAGE_CMD controlvm $VM savestate
	$VBOXMANAGE_CMD snapshot $VM restorecurrent
	$VBOXMANAGE_CMD startvm $VM
}

delete_snapshot()
{
	SNAPSHOT_COUNT=`$VBOXMANAGE_CMD snapshot $VM list | grep UUID | wc -l`
	if [ $SNAPSHOT_COUNT -gt 0 ]; then
		$VBOXMANAGE_CMD controlvm $VM savestate
		$VBOXMANAGE_CMD snapshot $VM delete ${SNAPSHOT_NAME}${SNAPSHOT_COUNT}
		SNAPSHOT_COUNT=$(($SNAPSHOT_COUNT-1))
		$VBOXMANAGE_CMD startvm $VM
	else
		echo "Actualmente no hay ningun snapshot".
	fi
}

list_snapshots()
{
	$VBOXMANAGE_CMD snapshot $VM list 
}


#######################  PARAMETROS  #####################

while getopts "hm:c:" opt; do
        case $opt in
                h)
                        usage
                        exit 1
                        ;;
                m)
                        #echo "-m was triggered!" >&2
                        VM=$OPTARG
                        ;;
                c)
                        #echo "-c was triggered!" >&2
                        ACTION=$OPTARG
                        ;;
                *)
                        echo "Invalid option: -$OPTARG" >&2
                        ;;
        esac
done


########################    MAIN    ######################

case $ACTION in
        take)
                if [ ! -z $VM ]; then
                        take_snapshot
                else
                        echo "Opcion -m debe estar definida."
                        usage
                        exit 1
                fi
                ;;
        restore)
                if [ ! -z $VM ]; then
                        restore_snapshot
                else
                        echo "Opcion -m debe estar definida."
                        usage
                        exit 1
                fi
                ;;
        delete)
                if [ ! -z $VM ]; then
                        delete_snapshot
                else
                        echo "Opcion -m debe estar definida."
                        usage
                        exit 1
                fi
                ;;
        list)
                if [ ! -z $VM ]; then
                        list_snapshots
                else
                        echo "Opcion -m debe estar definida."
                        usage
                        exit 1
                fi
                ;;
        *)
                echo "Opcion '$ACTION' no valida." 
                usage
                exit 1
                ;;
esac
